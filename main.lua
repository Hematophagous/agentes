require 'motor.interface'

local gerenciador = GerenciadorDeEstados()
local maxt = 0.1
local t = maxt

function love.load()
    gerenciador:load()
end

function love.update(dt)
    if t > 0 then
        t = t - dt 
    else
        gerenciador:update(dt)
        t = maxt
    end
    
end

function love.draw()
    gerenciador:draw()
end

function love.mousereleased(x, y, b)
    gerenciador:mousereleased(x, y, b)
end