require 'entidades.ambiente'
require 'entidades.agentes.agente_reativo_simples'
require 'entidades.agentes.agente_baseado_modelo'

function GerenciadorDeEstados()
    local gerenciador = {}
    
    function gerenciador:load()
        self.estados = {
            Menu = Menu(),
            Simulador = Simulador()
        }
        self:mudarEstado('Menu')
    end

    function gerenciador:mudarEstado(estado, argumento)
        self.estado_atual = self.estados[estado]
        self.estado_atual:load(argumento)
    end

    function gerenciador:update(dt)
        self.estado_atual:update(dt, self)
    end

    function gerenciador:draw()
        self.estado_atual:draw()
    end
    
    function gerenciador:mousereleased(x, y, b)
        if self.estado_atual.mousereleased then
            self.estado_atual:mousereleased(self, x, y, b)
        end
    end

    return gerenciador
end

function Simulador()
    local simulador = {}

    function simulador:load(tipo_agente)
        self.pausa = false
        self.ambiente = Ambiente(20, 20)
        self.ambiente:load()
        self.agente1 = tipo_agente[1](1, 1, self.ambiente)
        self.agente2 = tipo_agente[2](20, 1, self.ambiente)
        self.tempo_inicio = love.timer.getTime()
        self.tempo_decorrido = 0
    end

    function simulador:update(dt, manager)
        local l1 = #self.ambiente.matriz[self.ambiente.posL1.x][self.ambiente.posL1.y].conteudo.lixos
        local l2 = #self.ambiente.matriz[self.ambiente.posL2.x][self.ambiente.posL2.y].conteudo.lixos

        local Incinerador = #self.ambiente.matriz[self.ambiente.posIncinerador.x][self.ambiente.posIncinerador.y].conteudo.lixos
        local Recicladora = #self.ambiente.matriz[self.ambiente.posRecicladora.x][self.ambiente.posRecicladora.y].conteudo.lixos
        
        if Incinerador + Recicladora == 40 then
            -- self:salvar_relatorio()
            -- self.ambiente:mapa_temperatura()
            love.timer.sleep(1)
            manager:mudarEstado('Menu')
        elseif not self.pausa then
            self.agente1:update(dt)
            self.agente2:update(dt)
            self.tempo_decorrido =  love.timer.getTime() - self.tempo_inicio
        end
    end

    function simulador:draw()
        self.ambiente:draw()
        self.agente1:draw()
        self.agente2:draw()

        love.graphics.setColor(1, 1, 1)

        -- self:draw_text_block('Ações: '..self.agente1.moves, (self.ambiente.w + 2) * 20, 20, 100, 20)

        local str = 'Time: '..string.format("%.2f secs.", self.tempo_decorrido)
        self:draw_text_block(str, (self.ambiente.w + 2) * 20, 62, string.len(str)*6.5 + 2, 20)

        self:draw_text_block('Reciclaveis: '..self.ambiente.rec, (self.ambiente.w + 2) * 20, 100, 100, 20)
        self:draw_text_block('Organicos: '..self.ambiente.org, (self.ambiente.w + 2) * 20, 140, 100, 20)
        -- self:draw_text_block('Total: '..self.ambiente.blocos_sujos, (self.ambiente.w + 2) * 20, 180, 100, 20)

        local l1 = #self.ambiente.matriz[self.ambiente.posL1.x][self.ambiente.posL1.y].conteudo.lixos
        self:draw_text_block('Lixeira 1: '..l1, (self.ambiente.w + 2) * 20, 220, 100, 20)

        local l2 = #self.ambiente.matriz[self.ambiente.posL2.x][self.ambiente.posL2.y].conteudo.lixos
        self:draw_text_block('Lixeira 2: '..l2, (self.ambiente.w + 2) * 20 + 120, 220, 100, 20)

        local inc = #self.ambiente.matriz[self.ambiente.posIncinerador.x][self.ambiente.posIncinerador.y].conteudo.lixos
        self:draw_text_block('Incinerador: '..inc, (self.ambiente.w + 2) * 20, 260, 100, 20)

        local rec = #self.ambiente.matriz[self.ambiente.posRecicladora.x][self.ambiente.posRecicladora.y].conteudo.lixos

        self:draw_text_block('Recicladora: '..rec, (self.ambiente.w + 2) * 20 + 120, 260, 100, 20)


        -- self:draw_text_block('Lixo Identificado: '..self.agente1.lixo_identificado, (self.ambiente.w + 2) * 20, 300, 100, 20)

    end

    function simulador:draw_text_block(text, block_pos_x, block_pos_y, block_width, block_height)
        love.graphics.rectangle('line', block_pos_x, block_pos_y, block_width, block_height)
        love.graphics.print(text,  block_pos_x + 2, block_pos_y + 2)
    end

    function simulador:mousereleased(manager, x, y, b)
        self.pausa = not self.pausa
    end

    return simulador
end

function Menu()
    local menu = {}

    function menu:load()
        self.opcoes = {
            {texto ='Agente Reativo Simples', x = 32, y = 32, w = 148, h = 20, tipo = {ColetorReativoSimples, TratadorReativoSimples}},
            {texto ='Agente Baseado Em Modelo', x = 32, y = 64, w = 148, h = 20, tipo = {ColetorBaseadoEmModelo, TratadorReativoSimples}}
        }
    end

    function menu:update(dt)

    end

    function menu:draw()
        local x, y = love.mouse:getPosition()

        for i=1, #self.opcoes do 
            local opcao = self.opcoes[i]

            if x > opcao.x and x < opcao.x + opcao.w and y > opcao.y and y < opcao.y + opcao.h then
                love.graphics.setColor(1, 1, 1)
            else
                love.graphics.setColor(.7, .7, .7)
            end

            love.graphics.rectangle('line', opcao.x, opcao.y, opcao.w, opcao.h)
            love.graphics.print(opcao.texto, opcao.x + 2, opcao.y + 2)
        end
    end

    function menu:mousereleased(manager, x, y, b)
        for i=1, #self.opcoes do 
            local opcao = self.opcoes[i]
            if x > opcao.x and x < opcao.x + opcao.w and 
                y > opcao.y and y < opcao.y + opcao.h and b == 1 then
                manager:mudarEstado('Simulador', opcao.tipo)
            end
        end
    end

    return menu
end

