require 'entidades.agentes.agente_basico'

function ColetorReativoSimples(x, y, ambiente)
    local agente = AgenteBasico(x, y, ambiente)

    function agente:perceber()
        self.acoes = {}

        if not self.carregando then
            if type(self.ambiente.matriz[self.x][self.y].conteudo) == 'string' then
                table.insert(self.acoes, self.coletar)
            else
                local blocos_lixo = self:checar_lixo()

                if #blocos_lixo > 0 then
                    self:remover_organicos(blocos_lixo)
                    self:converter_acoes(blocos_lixo)
                else 
                    self:adicionar_caminhos_possiveis()
                end
            end
        else 
            if type(self.ambiente.matriz[self.x][self.y].conteudo) ~= 'table' or self.ambiente.matriz[self.x][self.y].conteudo.nome ~= 'LIX' then
                table.insert(self.acoes, self.andar_lixeira)
            else 
                table.insert(self.acoes, self.colocar)
            end
        end
    end

    function agente:adicionar_caminhos_possiveis()
        if self.x > 1 then
            table.insert(self.acoes, self.andar_esquerda)
        end
        if self.x < self.ambiente.w then
            table.insert(self.acoes, self.andar_direita)
        end
        if self.y > 1 then
            table.insert(self.acoes, self.andar_cima)
        end
        if self.y < self.ambiente.h then
            table.insert(self.acoes, self.andar_baixo)
        end
    end

    function agente:converter_acoes(blocos_lixo)
        for _, v in pairs(blocos_lixo) do
            if v.x > self.x then 
                table.insert(self.acoes, self.andar_direita)
            end
            if v.x < self.x then 
                table.insert(self.acoes, self.andar_esquerda)
            end
            if v.y > self.y then 
                table.insert(self.acoes, self.andar_baixo)
            end
            if v.y < self.y then 
                table.insert(self.acoes, self.andar_cima)
            end
        end
    end

    function agente:remover_organicos(blocos_lixo)
        local i = 1

        while blocos_lixo[i] and blocos_lixo[i].conteudo == 'rec' do i = i + 1 end
        if i > 1 then 
            for j=#blocos_lixo, i, -1 do 
                table.remove(blocos_lixo, j)
            end
        end
    end

    function agente:agir()
        if self.acoes and #self.acoes > 0 then 
            self.acoes[math.random(#self.acoes)](self)
        end
    end


    return agente
end

function TratadorReativoSimples(x, y, ambiente)
    local agente = AgenteBasico(x, y, ambiente)
    agente.lixeiras = {agente.ambiente.posL1, agente.ambiente.posL2}
    agente.tratadores = {org = agente.ambiente.posIncinerador, rec = agente.ambiente.posRecicladora}
    agente.objetivo = 1--agente.ambiente.posL1
    
    function agente:perceber()
        -- Aqui açoes deverá ser uma tabela com dois campos: um para a acao e outro para o objetivo
        self.acoes = {}
    
        if not self.carregando then
            -- print(self:comparar_posicao)
            if self:comparar_posicao(self.lixeiras, self.objetivo) then
                if #self.ambiente.matriz[self.x][self.y].conteudo.lixos > 0 then 
                    table.insert(self.acoes, self.coletar)
                else
                    self.objetivo = self.objetivo % #self.lixeiras + 1
                    table.insert(self.acoes, {self.andar_objetivo, self.objetivo})
                end
            else
                table.insert(self.acoes, {self.andar_objetivo, self.objetivo})
            end
        else
            if not self:comparar_posicao(self.tratadores, self.carregando) then 
                table.insert(self.acoes, {self.andar_objetivo, self.carregando})
            else
                table.insert(self.acoes, self.colocar)
            end
            -- if type(self.ambiente.matriz[self.x][self.y].conteudo) ~= 'table' or self.ambiente.matriz[self.x][self.y].conteudo.nome ~= 'LIX' then
            --     table.insert(self.acoes, self.andar_lixeira)
            -- else 
            --     table.insert(self.acoes, self.colocar)
            -- end
        end
    end

    function agente:comparar_posicao(tabela, indice)
        local lixeira = tabela[indice]
        return self.x == lixeira.x and self.y == lixeira.y
    end

    function agente:adicionar_caminhos_possiveis()
        if self.x > 1 then
            table.insert(self.acoes, self.andar_esquerda)
        end
        if self.x < self.ambiente.w then
            table.insert(self.acoes, self.andar_direita)
        end
        if self.y > 1 then
            table.insert(self.acoes, self.andar_cima)
        end
        if self.y < self.ambiente.h then
            table.insert(self.acoes, self.andar_baixo)
        end
    end

    function agente:converter_acoes(blocos_lixo)
        for _, v in pairs(blocos_lixo) do
            if v.x > self.x then 
                table.insert(self.acoes, self.andar_direita)
            end
            if v.x < self.x then 
                table.insert(self.acoes, self.andar_esquerda)
            end
            if v.y > self.y then 
                table.insert(self.acoes, self.andar_baixo)
            end
            if v.y < self.y then 
                table.insert(self.acoes, self.andar_cima)
            end
        end
    end

    function agente:remover_organicos(blocos_lixo)
        local i = 1

        while blocos_lixo[i] and blocos_lixo[i].conteudo == 'rec' do i = i + 1 end
        if i > 1 then 
            for j=#blocos_lixo, i, -1 do 
                table.remove(blocos_lixo, j)
            end
        end
    end

    function agente:agir()
        if self.acoes and #self.acoes > 0 then 
            local acao = self.acoes[math.random(#self.acoes)]

            if type(acao) == 'table' then
                if type(acao[2]) == 'number'then
                    acao[1](self, self.lixeiras[acao[2]])
                else 
                    acao[1](self, self.tratadores[acao[2]])
                end
            else
                acao(self)
            end
        end
        -- print(self.carregando)
    end
    
    function agente:draw()
        love.graphics.setColor(0, 0.4, .8)
        love.graphics.rectangle('fill', self.x * 20 + 1, self.y * 20 + 1, 18, 18)
    end

    return agente
end
