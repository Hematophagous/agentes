function AgenteBasico(x, y, ambiente)
    -- Classe com atributos e métodos genéricos dos agentes

    local agente = {
        x = x,                  -- posição em x
        y = y,                  -- posição em y
        ambiente = ambiente,    -- ambiente com o qual o agente vai interagir
        carregando = nil,       -- conteúdo que o cliente 
    }
    
    -- Métodos para movimentos básicos (Andar para cima, baixo direita e esquerda)
    -- Ideia era usar esse contador de visitas para definir uma boa rota para o agente, talvez isso pode sair
    function agente:andar_direita()
        self.x = self.x + 1
        self.ambiente.matriz[self.x][self.y].visitas = self.ambiente.matriz[self.x][self.y].visitas + 1
    end

    function agente:andar_esquerda()
        self.x = self.x - 1
        self.ambiente.matriz[self.x][self.y].visitas  = self.ambiente.matriz[self.x][self.y].visitas + 1
    end

    function agente:andar_baixo()
        self.y = self.y + 1
        self.ambiente.matriz[self.x][self.y].visitas  = self.ambiente.matriz[self.x][self.y].visitas + 1
    end

    function agente:andar_cima()
        self.y = self.y - 1
        self.ambiente.matriz[self.x][self.y].visitas  = self.ambiente.matriz[self.x][self.y].visitas + 1
    end
    -- Métodos para movimentos básicos (Andar para cima, baixo direita e esquerda)

    function agente:coletar()
        -- Método para coletar o conteúdo de algum lugar (pode ser o chão ou as lixeiras)
        local bloco = self.ambiente.matriz[self.x][self.y]

        if type(bloco.conteudo) == 'string' then    -- se conteudo for uma string ('org', ou 'rec'), é do chão
            self.carregando = self.ambiente:limpar_bloco(self.x, self.y)
        elseif type(bloco.conteudo) == 'table' then -- se for uma tabela, é uma lixeira
            self.carregando = bloco.conteudo:retirar()
        end
    end

    function agente:colocar()
        -- Método para por o lixo em alguma lixeira ou tratador de lixo
        local bloco = self.ambiente.matriz[self.x][self.y]

        if type(bloco.conteudo) == 'table' then 
            -- Só coloca se for uma lixeira, um incinerador ou uma recicladora 
            bloco.conteudo:colocar(self.carregando)
            self.carregando = nil
        end
    end

    function agente:andar_objetivo(objetivo)
        -- Dado um objetivo (Normalmente uma lixeira ou um tratador), método para andar um a bloco mais próximo deste.
        -- objetivo deve ter os atributos de posição (x, y)
        if objetivo.x > self.x then
            self:andar_direita()
        elseif objetivo.x < self.x then 
            self:andar_esquerda()
        elseif objetivo.y > self.y then 
            self:andar_baixo()
        elseif objetivo.y < self.y then 
            self:andar_cima()
        end
    end

    function agente:andar_lixeira()
        -- Método para localizar a lixeira mais próxima e definí-la como o objetivo
        local distancia_l1 = math.min(math.abs(self.x - self.ambiente.posL1.x) + math.abs(self.y - self.ambiente.posL1.y, 2))
        local distancia_l2 = math.min(math.abs(self.x - self.ambiente.posL2.x) + math.abs(self.y - self.ambiente.posL2.y, 2))

        if distancia_l1 < distancia_l2 then 
            self:andar_objetivo(self.ambiente.posL1)
        else 
            self:andar_objetivo(self.ambiente.posL2)
        end
    end

    function agente:andar_recicladora()
        -- Método para andar até a recicladora
        self:andar_objetivo(self.ambiente.posRecicladora)
    end

    function agente:andar_incinerador()
        -- Método para andar até o incinerador
        self:andar_objetivo(self.ambiente.posIncinerador)
    end

    function agente:checar_lixo()
        -- Método para verificar se há lixo nos 4 blocos adjascentes

        -- Variáveis para facilitar a leitura
        local matriz = self.ambiente.matriz         
        local up, down, left, right = matriz[self.x][self.y - 1], matriz[self.x][self.y + 1], matriz[self.x - 1], matriz[self.x + 1]
        -- Variáveis para facilitar a leitura

        local blocos_lixo = {} -- armazena os blocos que contém lixo

        if up and type(up.conteudo) == 'string' then
            table.insert(blocos_lixo, up)
        end
        if down then
            -- print(down.conteudo)
            if type(down.conteudo) == 'string' then
                table.insert(blocos_lixo, down)
            end
        end
        if left and type(left[self.y].conteudo) == 'string' then
            table.insert(blocos_lixo, left[self.y])
        end
        if right then
            if type(right[self.y].conteudo) == 'string' then
                table.insert(blocos_lixo, right[self.y])
            end
        end
        
        blocos_lixo = self:ordenar_prioridade(blocos_lixo) -- Blocos recicláveis devem ficar na frente da tabela

        return blocos_lixo
    end

    function agente:ordenar_prioridade(blocos)
        -- Método para ordenar a tabela de forma q lixo reciclável tenha maior prioridade que lixo organico
        for i=1, #blocos-1 do
            for j=i+1, #blocos do 
                if blocos[i].conteudo == 'org' and blocos[j].conteudo == 'rec' then
                    local aux = blocos[i]
                    blocos[i] = blocos[j]
                    blocos[j] = aux
                end
            end
        end

        return blocos
    end 

    function agente:update(dt)
        -- Looping do agente (esses métodos são obrigatórios e devem ser implementados nas subclasses)
        self:perceber()
        self:agir()
        -- O agente age de acordo com sua percepção atual do ambiente
    end

    function agente:draw()
        love.graphics.setColor(.9, 0, .4)
        love.graphics.rectangle('fill', self.x * 20 + 1, self.y * 20 + 1, 18, 18)
    end

    return agente
end