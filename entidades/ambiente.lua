require 'entidades.lixeiras'

function Bloco(i, j, conteudo)
    -- conteudo deve ser nil para chão limpo, 'reciclavel', 'organico', 
    -- Lixeira, Incinerador, Recicladora

    local bloco = {
        x = i,
        y = j,
        conteudo = conteudo,
        visitas = 0
    }

    function bloco:load_image()
        -- print()
        local image_name = ''

        if type(self.conteudo) == 'table' then 
            image_name = 'imagens/lixeiras/'..self.conteudo.nome..'.png'
        elseif type(self.conteudo) == 'string' then 
            image_name = 'imagens/'..(self.conteudo.nome or self.conteudo)..'/sprite-0'..math.random(4, 5)..'.png'
        end
        self.imagem = love.graphics.newImage(image_name)
    end



    function bloco:limpar()
        local conteudo = self.conteudo
        
        self.conteudo = nil 

        return conteudo
    end

    function bloco:draw()
        love.graphics.setColor(1, 1, 1)

        if self.conteudo and self.imagem then 
            love.graphics.draw(self.imagem, self.x * 20, self.y * 20)
        -- else
        --     love.graphics.rectangle('fill', self.x * 20 + 2, self.y * 20 + 2, 16, 16)
        end
    end

    return bloco
end

function Ambiente(largura, altura)
    -- Largura e altura em termos de celulas
    local ambiente = {
        w = largura,
        h = altura,
        matriz = {},

        rec = 0,    -- Número de reciclaveis restantes
        org = 0,    -- Número de orgânicos restantes

        posL1 = {x = 1, y = 12},
        posL2 = {x = 20, y = 12},

        posIncinerador = {x = 1, y = 20},
        posRecicladora = {x = 20, y = 20},
    }
    
    function ambiente:load()
        local grass_image_name = 'imagens/grass-01.png'
        self.grass_image = love.graphics.newImage(grass_image_name)

        for i=1, self.w do 
            self.matriz[i] = {}
            for j=1, self.h do 
                self.matriz[i][j] = Bloco(i, j)
            end
        end

        --lixeiras
        self.matriz[1][12].conteudo = Lixeira(self.posL1.x, self.posL1.y)
        self.matriz[1][12]:load_image()
        self.matriz[20][12].conteudo = Lixeira(self.posL2.x, self.posL2.y)
        self.matriz[20][12]:load_image()

        self.matriz[1][20].conteudo = Incinerador(self.posIncinerador.x, self.posIncinerador.y)
        self.matriz[1][20]:load_image()
        self.matriz[20][20].conteudo = Recicladora(self.posRecicladora.x, self.posRecicladora.y)
        self.matriz[20][20]:load_image()

        self:sujar(38)
    end

    function ambiente:sujar(i)
        local tiposLixo = {'org', 'rec'}

        while i > 0 do
            local x = math.random(1, self.w)
            local y = math.random(1, self.h)
            local bloco = self.matriz[x][y]
            
            if not (x == 1 and y == 1) and not (x == 20 and y == 1) then 
                if not bloco.conteudo then 
                    bloco.conteudo = tiposLixo[math.random(#tiposLixo)]
                    bloco:load_image()

                    self[bloco.conteudo] = self[bloco.conteudo] + 1 -- Incrementa o contador de lixo de cada tipo
                    i = i - 1
                end
            end
        end

    end

    function ambiente:draw()
        if love then

            self:draw_love()
        else 
            self:draw_console()
        end
    end 

    function ambiente:limpar_bloco(x, y)
        local conteudo = self.matriz[x][y].conteudo 
        self.matriz[x][y]:limpar()
        self[conteudo] = self[conteudo] - 1

        return conteudo
    end

    function ambiente:draw_love()
        -- Deve funcionar apenas no espaço do love2D
        for i=1, self.w do
            for j=1, self.h do
                local bloco = self.matriz[i][j]
                
                love.graphics.setColor(1, 1, 1)

                local s = ''
                if bloco.conteudo == 'rec' then 
                    s = 'R'
                elseif bloco.conteudo == 'org' then 
                    s = 'X'
                else 
                    s = bloco.visitas
                end

                love.graphics.print(s, i * 20, j * 20)
                love.graphics.draw(self.grass_image, i * 20, j * 20)
                bloco:draw()
            end
        end
    end

    function ambiente:draw_console()
        -- Deve funcionar apenas no espaço do love2D
        for i=1, self.w do
            for j=1, self.h do
                local bloco = self.matriz[j][i]    -- eixos se invertem por causa da ordem de impressão
                local conteudo = bloco.conteudo
                if conteudo and type(conteudo) ~= 'string' then 
                    conteudo = conteudo.nome
                end

                io.write((conteudo or ' - ')..'\t')
            end
            print()
        end        
    end

    
    -- function ambiente:mapa_temperatura()
    --     local file = love.filesystem.newFile('mapa_temperatura.csv')
    --     file:open('w')

    --     for i=1, self.w do
    --         for j=1, self.h do
    --             file:write(self.matriz[i][j].visitas..',')
    --         end
    --         file:write('\n')
    --     end
    --     file:close()
    -- end

    return ambiente
end