function Lixeira(x, y, nome)
    local lixeira = {
        x = x,
        y = y,
        nome = nome or 'LIX',
        lixos = {}
    }
    
    function lixeira:colocar(lixo)
        table.insert(self.lixos, lixo)
    end

    function lixeira:retirar()
        return table.remove(self.lixos, #self.lixos)
    end

    return lixeira
end

function Incinerador(x, y)
    local inc = Lixeira(x, y, 'INC')

    function inc:colocar(lixo)
        if lixo == 'org' then 
            table.insert(self.lixos, lixo)
        end
    end

    return inc
end

function Recicladora(x, y)
    local rec = Lixeira(x, y, 'REC')

    function rec:colocar(lixo)
        if lixo == 'rec' then 
            table.insert(self.lixos, lixo)
        end
    end

    return rec
end